<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=> 'session'], function(){
    // requester : sites
    Route::post('add', 'SessionController@add');
    Route::post('update/{session_id}', 'SessionController@update');
    Route::post('update-by-roomid/{room_id}', 'SessionController@updateByRoomId');
    Route::post('get/status/{session_id}', 'SessionController@getStatus');

    // requester : bot
    Route::post('list-recorded-video', 'SessionController@getRecordedSession');
    Route::post('get', 'SessionController@get');
    Route::post('test/{count}', 'SessionController@test');
    Route::post('set/room-url/{base_url}/{course_id}/{session_id}', 'SessionController@setRoomUrl');
    Route::post('get-sessions/{room_id}', 'SessionController@getSessionsByRoom');
    // requester : bot
    Route::post('set/recording', 'SessionController@setRecording');
    Route::post('set/recorded', 'SessionController@setRecorded');
    Route::post('set/uploading', 'SessionController@setUploading');
    Route::post('set/uploaded', 'SessionController@setUploaded');

});
