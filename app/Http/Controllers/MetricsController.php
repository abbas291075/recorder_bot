<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;

class MetricsController extends Controller
{
    private $_client;
    private $namespace = 'recorder';
    public function __construct()
    {
        $this->_client = new \Prometheus\Client('/metrics');
    }
    public function index(){
        $this->getCounterRecording();
        $this->getCounterRecorded();
        $this->getCountWaitToRecord();
        echo $this->_client->serialize();
    }

    private function getCounterRecording(){
        $counter = $this->_client->newCounter([
            'namespace' => $this->namespace,
            'subsystem' => 'recording',
            'name' => 'counter',
            'help' => 'Get Count Classes that now recording...'
        ]);

        $counter->increment(['type' => 'all'], Session::where('status', Session::STATUS_DO_RECORDING)->count());
    }

    private function getCounterRecorded(){
        $counter = $this->_client->newCounter([
            'namespace' => $this->namespace,
            'subsystem' => 'recorded',
            'name' => 'counter',
            'help' => 'Get Count Classes that recorded'
        ]);

        $counter->increment(['type' => 'all'], Session::where('status', Session::STATUS_SUCCESS_RECOREDED)->count());
    }

    private function getCountWaitToRecord(){
        $counter = $this->_client->newCounter([
            'namespace' => $this->namespace,
            'subsystem' => 'waiting_to_record',
            'name' => 'counter',
            'help' => 'Get Count Classes that recorded'
        ]);

        $counter->increment(['type' => 'all'], Session::where('status', Session::STATUS_WAIT_RECORD)->count());
    }
}
