<?php

namespace App\Http\Controllers;
use App\Session;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\ResponseObject;
use \Illuminate\Support\Facades\Response as FcadeResponse;
class SessionController extends Controller
{
    public function add(request $request){
        $response = new ResponseObject();
        $validator = Validator::make($request->json()->all(), [
            'title'      => 'required',
            'description'=> 'required',
            'base_url'    => 'required',
            'room_url'    => 'required',
            'start_time'  => 'required',
            'course_id'   => 'required',
            "session_id"  => 'required',
            'room_id' => 'required',
            'service' => 'required'
        ]);
        if($validator->fails()){
            $response->status = $response::status_fail;
            $response->code   = $response::code_unauthorized;
            foreach($validator->errors()->getMessages() as $err){
                $response->setMessages($err);
            }
        }else{
            $sessionIsExists = Session::where('course_id', $request->all()['course_id'])->where('session_id', $request->all()['session_id'])->first();
            if(!$sessionIsExists){
                $newSession = Session::create($request->all());
                if($newSession){
                    $response->status = $response::status_ok;
                    $response->code   = $response::code_ok;
                }else{
                    $response->status = $response::status_fail;
                    $response->code   = $response::code_failed;
                }
            }
            else{
                $sessionIsExists->status = Session::STATUS_WAIT_RECORD;
                if($sessionIsExists->save()){
                    $response->status = $response::status_ok;
                    $response->code   = $response::code_ok;
                }else{
                    $response->status = $response::status_fail;
                    $response->code   = $response::code_failed;
                }
            }
        }

        return FcadeResponse::json($response);
    }

    public function update(Request $request, $session_id){
        // $validator = Validator::make($request->json()->all(), [
        //     'title'      => 'required',
        //     'description'=> 'required',
        //     'base_url'    => 'required',
        //     'room_url'    => 'required',
        //     'start_time'  => 'required',
        //     'course_id'   => 'required',
        // ]);
        $response = new ResponseObject();


        $session = Session::where('session_id', $session_id)
                          ->where('base_url', $request->all()['base_url'])
                          ->get()
                          ->first();
        if($session instanceof Session){
//             $req = $request->all();
//             $req['start_time'] = time();
//
//             $saved = $session->update($req);
//             if($saved){
//                 return $saved;
//                 $response->status = $response::status_ok;
//                 $response->code   = $response::code_ok;
//             }else{
//                 $response->status = $response::status_fail;
//                 $response->code   = $response::code_failed;
//             }
            foreach($request->all() as $key => $field){
               $session->{$key} = $field;
            }
            $session->updated_at = time();
            $session->start_time = time();

            $saved = $session->save();
            if($saved){
                $response->status = $response::status_ok;
                $response->code   = $response::code_ok;
            }else{
                $response->status = $response::status_fail;
                $response->code   = $response::code_failed;
            }
        }else{
            $response->status = ResponseObject::status_fail;
            $response->code   = ResponseObject::code_not_found;
            $response->setMessages('جلسه مورد نظر یافت نشد');
        }



        return FcadeResponse::json($response);

    }

    public function updateByRoomId(Request $request, $room_id){

        $response = new ResponseObject();


        $session = Session::where('room_id', $room_id)
            ->get()
            ->first();
        if($session instanceof Session){

            foreach($request->all() as $key => $field){
                $session->{$key} = $field;
            }
            $session->updated_at = time();
            $session->start_time = time();

            $saved = $session->save();
            if($saved){
                $response->status = $response::status_ok;
                $response->code   = $response::code_ok;
            }else{
                $response->status = $response::status_fail;
                $response->code   = $response::code_failed;
            }
        }else{
            $response->status = ResponseObject::status_fail;
            $response->code   = ResponseObject::code_not_found;
            $response->setMessages('جلسه مورد نظر یافت نشد');
        }



        return FcadeResponse::json($response);
    }
    public function get(Request $request){
        /*
         * @var
         */
        $params = $request->all();
        $sess = [];
        /**
         comment by Abbas Jafari:
         * after extract params access to vars:
         * @var $time
         * @var $IP
         * @var $service
         * @var $space
         * 2021-02-27, Sat, 11:50
         */

        extract($params);
        $ip = $IP;
        $sessions = Session::where('service', $service)->where('status', Session::STATUS_RETTERY_RECORD);

        if($time){
            $time = time() - (int) $time * 60;
            $sessions->where('start_time', '>', $time);
        }

        $sessionsIp = clone($sessions);
        $sessionsIp->where('reserve_by', $ip)->limit($space);
        $sess = $sessionsIp = array_merge($sess, $sessionsIp->get()->toArray());
        $countSessionIps = count($sessionsIp);
        if($countSessionIps < $space){
            $space = $space - $countSessionIps;
            $sessions->limit($space);
            $sessions->where('reserve_by', null);
            $sess = array_merge($sess, $sessions->get()->toArray());
            $sessions->update(['reserve_by' => $ip]);
        }



//        if(!$time){
//            $sessions = Session::where('status', Session::STATUS_RETTERY_RECORD)->get()->toArray();
//        }else{
//            $time = time() - (int) $time * 60; // convert time to sec
//            $sessions = Session::where('start_time', '>', $time)->where('start_time', '<', time())->where('status', Session::STATUS_RETTERY_RECORD)->get()->toArray();
//        }
        $response = new ResponseObject();
        $response->status = ResponseObject::status_ok;
        $response->code   = ResponseObject::code_ok;
        if($sess)
            $response->setResult($sess);

        return FcadeResponse::json($response);
    }

    public function test(Request $request, $count){
      $sessions = Session::where('base_url','taleghani6.ir')->skip(0)->take($count)->get()->toArray();
        $response = new ResponseObject();
        $response->status = ResponseObject::status_ok;
        $response->code   = ResponseObject::code_ok;
        if($sessions)
            $response->setResult($sessions);

        return FcadeResponse::json($response);

    }

    public function getRecordedSession(Request $request){
        $sessions = Session::where('status', Session::STATUS_SUCCESS_RECOREDED)->get()->toArray();
        $response = new ResponseObject();
        $response->status = ResponseObject::status_ok;
        $response->code   = ResponseObject::code_ok;

        if($sessions)
            $response->setResult($sessions);

        return FcadeResponse::json($response);
    }


    public function getStatus(Request $request, $session_id){
        $validator = Validator::make($request->json()->all(), [
            'base_url' => 'required',
        ]);

        $response = new ResponseObject();
        if($validator->fails()){
            $response->status = $response::status_fail;
            $response->code   = $response::code_unauthorized;
            foreach($validator->errors()->getMessages() as $err){
                $response->setMessages($err);
            }
        }else{
            $session = Session::where('session_id', $session_id)->where('base_url', $request->all()['base_url'])->first();
            if($session){
                $session = $session->toArray();
                $response->status = $response::status_ok;
                $response->code   = $response::code_ok;
                $response->setResult($session);
            }
            else
            {
                $response->status = $response::status_fail;
                $response->code   = $response::code_failed;
            }

            return FcadeResponse::json($response);

        }

            return FcadeResponse::json($response);
    }


    public function setRecording(Request $request){
        $response = $this->setAction($request, Session::STATUS_DO_RECORDING);
        return $response;
    }

    public function setRecorded(Request $request){
        $response = $this->setAction($request, Session::STATUS_SUCCESS_RECOREDED);
        return $response;
    }

    public function setUploading(Request $request){
        $response = $this->setAction($request, Session::STATUS_DO_UPLOADING);
        return $response;
    }

    public function setUploaded(Request $request){
        $response = $this->setAction($request, Session::STATUS_SUCCESS_UPLOADED);
        return $response;
    }


    private function setAction(Request $request, $action){
        $validator = Validator::make($request->json()->all(), [
            'room_id' => 'required',
        ]);
        $response = new ResponseObject();
        if($validator->fails()){
            $response->status = $response::status_fail;
            $response->code   = $response::code_unauthorized;
            foreach($validator->errors()->getMessages() as $err){
                $response->setMessages($err);
            }
        }else{
            $room_id = $request->all()['room_id'];
            $session = Session::find($room_id);

            if($session instanceof Session){
                $session->status = $action;
                if($session->save()){
                    $response->status = $response::status_ok;
                    $response->code   = $response::code_ok;
                }else{
                    $response->status = $response::status_fail;
                    $response->code   = $response::code_failed;
                }
            }else{
                $response->status = ResponseObject::status_fail;
                $response->code   = ResponseObject::code_not_found;
                $response->setMessages('جلسه مورد نظر یافت نشد');
            }
        }

        return FcadeResponse::json($response);
    }

    public function setRoomUrl(Request $request, $base_url, $course_id, $session_id){
        $validator = Validator::make($request->json()->all(), [
            'new_url' => 'required',
        ]);
        $session = Session::where('session_id', $session_id)
                          ->where('course_id', $course_id)
                          ->where('base_url', $base_url)
                          ->first();
        $response = new ResponseObject();
        if($validator->fails()){
            $response->status = $response::status_fail;
            $response->code   = $response::code_unauthorized;
            foreach($validator->errors()->getMessages() as $err){
                $response->setMessages($err);
            }
        }else {
            if ($session instanceof Session) {
                $new_url_room = $request->all()['new_url'];
                $session->room_url = $new_url_room;
                if ($session->save()) {
                    $response->status = $response::status_ok;
                    $response->code = $response::code_ok;
                } else {
                    $response->status = $response::status_fail;
                    $response->code = $response::code_failed;
                }
            } else {
                $response->status = ResponseObject::status_fail;
                $response->code = ResponseObject::code_not_found;
                $response->setMessages('جلسه مورد نظر یافت نشد');
            }
        }
        return FcadeResponse::json($response);

    }

    public function getSessionsByRoom(Request $request, $room_id){
        $sessions = Session::where('room_id', $room_id)->get()->toArray();
        $response = new ResponseObject();
        $response->status = ResponseObject::status_ok;
        $response->code   = ResponseObject::code_ok;
        if($sessions)
            $response->setResult($sessions);

        return FcadeResponse::json($response);
    }
}
