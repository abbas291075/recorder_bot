<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    const STATUS_WAIT_RECORD = 0;
    const STATUS_DO_RECORDING = 7;
    const STATUS_SUCCESS_RECOREDED = 6;
    const STATUS_RETTERY_RECORD = 5;


    const STATUS_DO_UPLOADING = 11;
    const STATUS_SUCCESS_UPLOADED = 12;
    public $table = 'session';
    protected $fillable = [
        'title',
        'room_id',
        'description',
        'base_url',
        'room_url',
        'start_time',
        'course_id',
        'session_id',
        'reserve_by',
        'service'
    ];
}
